package com.sonalake.cognito.preauth.validator;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import org.slf4j.Logger;

import java.io.*;
import java.nio.charset.Charset;
import java.util.HashMap;

import static org.slf4j.LoggerFactory.getLogger;

public class CognitoPreAuthValidator implements RequestStreamHandler {
    private final Logger logger = getLogger(getClass());

    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Override
    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {
        LambdaLogger logger = context.getLogger();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("US-ASCII")));
        PrintWriter writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outputStream, Charset.forName("US-ASCII"))));
        try {
            HashMap event = gson.fromJson(reader, HashMap.class);
            logger.log("STREAM TYPE: " + inputStream.getClass().toString());
            logger.log("EVENT TYPE: " + event.getClass().toString());
            logger.log("EVENT :" + event);

//            EVENT :{request={userAttributes={sub=21960961-d526-436c-abb2-02429a330f66, email_verified=true, cognito:user_status=FORCE_CHANGE_PASSWORD, email=jedrzej.serwa@sonalake.com}, validationData=null, userNotFound=false}, callerContext={awsSdkVersion=aws-sdk-unknown-unknown, clientId=29hlc4csnfd4udf249mlghpmtj}, response={}, region=eu-west-1, userName=21960961-d526-436c-abb2-02429a330f66, triggerSource=PreAuthentication_Authentication, version=1, userPoolId=eu-west-1_wMUBvSgRR}

            writer.write(gson.toJson(event));
            if (writer.checkError()) {
                logger.log("WARNING: Writer encountered an error.");
            }

//            AWSCognitoIdentityProvider awsCognitoIdentityProvider = AWSCognitoIdentityProviderClientBuilder.standard()
//                    .build();
//
//            AdminListGroupsForUserRequest adminListGroupsForUserRequest = new AdminListGroupsForUserRequest();
//            adminListGroupsForUserRequest.setUsername(null);
//            AdminListGroupsForUserResult adminListGroupsForUserResult = awsCognitoIdentityProvider.adminListGroupsForUser(adminListGroupsForUserRequest);
//
//            AdminGetUserRequest adminGetUserRequest = new AdminGetUserRequest();
//            adminGetUserRequest.setUserPoolId(null);
//            adminGetUserRequest.setUsername(null);
//            AdminGetUserResult adminGetUserResult = awsCognitoIdentityProvider.adminGetUser(adminGetUserRequest);

        } catch (IllegalStateException | JsonSyntaxException exception) {
            logger.log(exception.toString());
        } finally {
            reader.close();
            writer.close();
        }
    }
}


//    @Override
//    public String handleRequest(Request input, Context context) {
//        logger.info("Retrieved request {}", input);
//        logger.info("Retrieved AWS Lambda context {}", context);
//
//        AWSCognitoIdentityProvider awsCognitoIdentityProvider = AWSCognitoIdentityProviderClientBuilder.standard()
//
//                .build();
//
//        AdminGetUserRequest adminGetUserRequest = new AdminGetUserRequest();
//        adminGetUserRequest.setUserPoolId("eu-west-1_wMUBvSgRR");
//        adminGetUserRequest.setUsername("jedrzej.serwa@sonalake.com");
//        AdminGetUserResult adminGetUserResult = awsCognitoIdentityProvider.adminGetUser(adminGetUserRequest);
//
//        logger.info("User result {}", adminGetUserResult);
//
//        return "200 OK";
//    }

